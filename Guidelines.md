# Bug Wrangler's Guide

The bug wrangling team organizes and prioritizes issues once they have been created. This guide provides:

1. An overview of the Inkscape bugtrackers and bug wrangling team,
2. A breakdown of some bug wrangling tasks, and
3. Some tips to help with testing and investigating an issue.

If you'd just like to report an issue, you can open a report [here in the Inbox](https://gitlab.com/inkscape/inbox/-/issues/new). See the [bug reporting guidelines](https://inkscape.org/contribute/report-bugs) for more details.

## Table of Contents

### Section 1: Overview

1. [Bug Wrangling Process](#bug-wrangling-process)
1. [Overview of Bugtrackers](#overview-of-bugtrackers)
1. [Bug Wrangling](#bug-wrangling)

### Section 2: Tasks

1. [Testing the Issue](#testing-the-issue)
1. [Addressing Feature Requests](#addressing-feature-requests)
1. [Addressing UX Issues](#addressing-ux-issues)
1. [Labeling Issues](#labeling-issues)
1. [Milestones](#milestones)
1. [Separating Out Issues](#separating-out-issues)
1. [Confidential Issues](#confidential-issues)
1. [Moving Issues](#moving-issues)
1. [Closing Issues](#closing-issues)
1. [Providing Support](#providing-support)
1. [Migrating Bugs](#migrating-bugs)

### Section 3: Tips and Tricks

1. [Searching](#searching)
1. [Formatting](#formatting)
1. [Quick Actions](#quick-actions)
1. [Checking User Preferences](#checking-user-preferences)
1. [Collecting Backtraces](#collecting-backtraces)
1. [Running Multiple Instances](#running-multiple-instances)

# Overview

## Bug Wrangling Process

We use multiple bugtrackers to organize the bugs that affect Inkscape, its extensions, dependencies, and related projects. To simplify things, we ask users to open issues in the Inbox.

From there, the issue is [confirmed](#testing-the-issue) and [labeled](#labeling-issues). It is confirmed when another user (possibly the bug wrangler) replicates the issue. If the issue isn't a duplicate of an existing issue, it is tidied up and moved to the appropriate bugtracker. It is then [labeled again and prioritized](#labeling-issues).

Not all issues are simply bugs though. [Feature requests](#addressing-feature-requests) are generally kept in the Inbox. [UX issues](#addressing-ux-issues) are also kept in the Inbox until a UX decision is made, though the UX team may move it to the UX bugtracker if they're working on the issue. [Support questions](#providing-support) are also kept in the Inbox.

```plantuml
@startuml
<style>
activityDiagram {
  HorizontalAlignment center
  WordWrap 200
  MaximumWidth 200
  FontSize 14
}
</style>
start
:Create issue in the Inbox;
:Label issue;

switch (Issue type?)
case ( Bug )
  :Confirm issue
  (by another user, possibly bug wrangler);
  #00aaee44:Move to relevant bugtracker;
  #00ee0044:Issue fixed;

case ( Feature Request )
  :Define scope/feature set;
  if (Related to Inkscape or Inkview, with plans made to implement?) then (yes)
    #00aaee44:Move to Inkscape bugtracker;
  else (no)
  endif
  #00ee0044:Feature implemented,
  or decide not to implement;

case ( UX )
  if (Actively researched by UX team?) then (yes)
    #00aaee44:Move to the Inkscape UX bugtracker;
  else (no)
  endif
  :Discuss and/or research;
  :Consensus built or UX decision made;
  #00aaee44:Move issue to relevant bugtracker;
  #00ee0044:UX change made;

endswitch
stop
@enduml
```

## Overview of Bugtrackers

Most issues are moved to the Inkscape or Extensions bugtracker once confirmed. The rest are kept in the Inbox or moved to a different bugtracker. Some common bugtrackers are listed below:

- [Inkscape](https://gitlab.com/inkscape/inkscape/): issues related to the Inkscape (and Inkview) app. Includes the internal c++ extensions, like the raster extensions and some of the import/export extensions (e.g. pdf, cdr, wmf, emf). Most packaged extensions can be found in the extensions project.
- [Extensions](https://gitlab.com/inkscape/extensions/): issues related to most Inkscape extensions. Aside from the Inkscape bugtracker, the remaining extensions are kept in the [extras group project](https://gitlab.com/inkscape/extras).
- [Inkscape UX](https://gitlab.com/inkscape/ux/): issues actively researched by the UX team, see [Addressing UX Issues](#addressing-ux-issues).
- [inkscape-web](https://gitlab.com/inkscape/inkscape-web/): issues related to [inkscape.org](https://inkscape.org). Issues are usually created and confirmed inside this bugtracker, instead of being moved to the Inbox. However, content-related issues should be moved to the bugtracker related to the content.
- [lib2geom](https://gitlab.com/inkscape/lib2geom/): issues related to the lib2geom dependency. Like other dependencies, issues are created and confirmed in the relevant bugtracker, instead of being opened in the Inbox. If an Inkscape bug requires a lib2geom fix first, it should still be tracked in Inkscape, with a separate issue created in lib2geom.
- [themes](https://gitlab.com/inkscape/themes/): issues related to the themes listed in the [themes repository](https://gitlab.com/inkscape/themes/).
- [inkscape-docs](https://gitlab.com/inkscape/inkscape-docs/): issues related to documentation (see sub-project descriptions).

For a complete list, see https://gitlab.com/inkscape/. Details for each project can be found in the project description. The existing issues in the bugtracker, or the contents of the repository may also provide hints. If in doubt, ask in the [chat](https://chat.inkscape.org/channel/team_testing).

## Bug Wrangling

The bug wrangling team organizes and prioritizes the issues in the Inkscape bugtrackers. This involves any of the tasks detailed in the [next section](#tasks).

You can find the team at the [team testing chat](https://chat.inkscape.org/channel/team_testing). Most of our discussion happens there. ~meta issues in the Inbox can also be used to track tasks and bug wrangling issues. GitLab's issues are better for tracking updates/decisions (particularly for us bug wranglers), while the chat is more suitable to live discussions.

### Joining the Bug Wrangling Team
To join, try [testing some issues](#testing-the-issue) to gain familiarity, then ask to join in the [team testing chat](https://chat.inkscape.org/channel/team_testing).

This does not require a long-term commitment to bug wrangling (though it wouldn't hurt), nor does it require you to be familiar with every task in this guide.

When you join, you'll be added to the [bug wrangling team on GitLab](https://gitlab.com/groups/inkscape/bug-wranglers/-/group_members ), which gives you the permissions to move and close issues. You'll also be asked to join the [bug wrangling team on inkscape.org](https://inkscape.org/*issue/ ). There aren't any extra requirements for joining either group.

If you have any issues or questions, don't hesitate to ask in the [team testing chat](https://chat.inkscape.org/channel/team_testing).

# Tasks

## Testing the Issue
### 1. Check for Duplicates

Before testing, **check for similar or duplicate issues**, see [Searching](#searching) for tips.

Similar issues may provide some extra insight into the issue. Mention the related issue in a comment or mark the issues as related (see [Quick Actions](#quick-actions)). If the issue is a duplicate, it should be [closed](#closing-issues).

### 2. Testing
If the issue isn't a duplicate, try to follow the steps to replicate.

**Post your results** in a comment, including your Inkscape version, OS, and any other relevant information. This allows others to see that the bug is reproducible and may narrow down the problem. Please spend a reasonable amount of time trying to replicate the issue before declaring that you can't replicate the issue.

You should be able to replicate the issue with default preferences, or identify the preferences required to trigger the issue. See [Checking User Preferences](#checking-user-preferences).

**Ask for missing or useful information** if you can't replicate the issue. Label the issue as ~"needs info" if there isn't enough information to triage further without the original user's input. The [last section of this guide](#tips-and-tricks) provides some debugging and troubleshooting that may help with replicating the issue.

### 3. Confirming the issue
Once confirmed, the issue should be cleaned up and moved to the relevant bugtracker. It's enough that another user replicates the issue, though you should confirm that the users are experiencing the same issue and their symptoms match up.

### 4. Clean up the issue

Issues should be clear and concise, so developers (and other users) can quickly find and read the issue.

- The title should be descriptive. The problem should be clear even before reading the description.
- The description should describe the problem in more detail. It should include:
    - Relevant logs and other information requested in the issue template, and
    - Relevant details taken from discussions in the comments or elsewhere.
- The description may be reworded and reformatted.
    - Sometimes the issue isn't clear, or can be simplified after the investigation. Clarify so it is easy to understand on the first read.
    - Issues should follow the issue template so that the relevant details can be found quickly.
    - Fix broken lists, links, headers, or other formatting problems (see [GitLab's formatting guide](https://gitlab.com/help/user/markdown)).
- Issues should be in English. If using a translator, it may help to keep a copy of the original text.
- Videos and images should have an equivalent text description. Developers tend to skip over videos as they are often lengthy and unclear.
- Extra bugs or suggestions should be moved off to separate issues so they can be searched and tracked separately, see [Separating Out Issues](#separating-out-issues).

### Etiquette
Please be polite. Users take quite a bit of effort to report an issue, and are likely unfamiliar with our bug wrangling system. They are more likely to be receptive if you are polite.

If you're in a bad mood or if the discussion is unproductive, take a break from the issue. You can come back to the issue later, or not at all! You don't need to handle every issue.

## Addressing Feature Requests
Feature requests include new features, new modes or behaviors for existing features, and SVG features that we don't support yet.

### Implementing the Feature Request
New features can take time to be added into Inkscape, even features that everyone wants. Typically, the bottleneck is finding a developer who is willing to devote a lot of time and effort towards implementing the feature. This may also require some collaboration with the user and UX team. Even then, features won't be introduced into bugfix releases, or in fast approaching releases (the cut-off point is the feature freeze before the alpha release).

We are not actively working through feature requests. Still, issues are a good place to keep track of discussions and updates.

You can suggest feature requests during developer meetings. You might be able to pique the interest of a developer, or receive some useful information about the feature (feasibility, or knowledge of previous attempts) that could be shared in the issue. Anyone is welcome to join, see [the calendar](https://inkscape.org/cals/) for the next meeting. There will be a link in the [developer chat](https://chat.inkscape.org/channel/team_devel) shortly before the meeting.

Some guidelines for pushing for a feature can be found in the draft, "[How to contribute to the Inkscape user experience](https://gitlab.com/inkscape/vectors/content/-/issues/44)".

### Improving the Feature Request
An issue is more likely to be found if the title is descriptive. Similarly, a clear and polished description may attract more user support and pique the interest of a developer.

Depending on the feature, it helps to include:

- Use cases for the feature,
- Support from other users,
- A detailed description of the feature,
- A well-defined scope so a minimal viable product can be created, or
- Screenshots, mock-ups, or prototypes.

Issues should be labeled with ~"feature request" and any other relevant labels (e.g. ~UI and ~Menus for a request to add a new button to a menu). Once a report is well defined, it can be given the ~improvement label too. This indicates the feature is well defined and can be worked on immediately, but doesn't mean that the feature will be developed.

**Guide users through the process** of improving their feature request. It is important to set proper expectations. Importantly, all the above information doesn't guarantee that a feature will be implemented. Gathering community support or finding a developer both make it much more likely to be implemented. Still, a polished feature request is more attractive, and the request must be well defined before it is actually implemented.

### Where Should This Feature Request Go?
Most feature requests remain in the Inbox, though some issues may be moved.

- If there is a plan to implement a feature in Inkscape and the feature is well defined, the issue may be moved to Inkscape bugtracker. It is not enough that people agree that the feature is nice to have. A developer should have committed to implementing the feature.
- Some issues may be more bug than feature. They may be treated like a bug and moved directly to Inkscape. This is ultimately a judgment call.
- Some of our projects track feature requests inside their own bugtrackers, so feature requests can be moved there.

Whether the issue is being tracked in the Inbox or Inkscape is less important than finding a developer to implement the request. We already have too many issues in both bugtrackers. Developers prefer that feature requests stay in the Inbox though, with the above exceptions.

## Addressing UX Issues

UX issues **remain in the Inbox** until a UX decision is made. The UX team may move an issue to the [UX bugtracker](https://gitlab.com/inkscape/ux/) when they are being actively discussed. Issues are moved to the [Inkscape](https://gitlab.com/inkscape/inkscape/) or other relevant bugtracker when there is consensus on the solution.

UX issues should be labeled ~UX and ~"needs UX decision".

Some issues can be treated like non-UX bugs, and moved. This may be the case if they are non-controversial, or an unintended regression from behavior in previous Inkscape versions.


## Labeling Issues
Labeling helps improve the search for issues massively, as it narrows down the search results significantly. Importance labels should be added to issues in the [Inkscape bugtracker](https://gitlab.com/inkscape/inkscape/-/issues) when they are moved there. They help with finding and prioritizing issues, especially useful near a release. [Milestones](#milestones) are also useful for prioritization.

### What labels are there?

The Inbox and Inkscape issues are the most important to label, as they have the most issues and labels. Most labels are shared between these two bugtrackers.

The most important labels for the Inkscape bugtracker are those prioritizing issues (found at the top of [this list](https://gitlab.com/inkscape/inkscape/-/labels)). The prioritization criteria is given in the label description (found by hovering over a label, or viewing the list). Priorities may be bumped up if multiple people report the same issue.

These Inbox-specific labels should always be used:

- ~Inkscape, ~extensions, ~website, etc. roughly indicate the bugtracker to move the issue to. Some issues relate to extensions kept in the Inkscape bugtracker. They can be labeled with both the ~Inkscape and ~extensions label.
- ~"feature request", ~improvement, ~bug, ~"support request / question", and ~UX indicate the type of issue.

There are quite a few more labels, mostly shared between the Inbox and Inkscape bugtrackers. They can be roughly organized as follows:

- Inkscape feature affected
    -  ~Dialogs, and the `Dialog::*` labels for each dialog
    -  ~Project::SVG2, and `Project::*` labels for recent features or GSOC projects
    -  ~Tool::Calligraphy, and `Tool:*` labels for each tool (e.g. select, node, rectangle)
    -  ~Input::Shortcuts, ~Input::Touchpad, and `Input::*` labels for certain input methods
    -  ~UI for Inkscape/Inkview's graphic user interface (e.g. widgets, icons, menus)
    -  ~text, ~CSS, ~Snapping, ~canvas, ~"open+import", ~translations, ~rendering, and other similarly colored labels
- Type of problem
    - ~regression for bugs that weren't present in older versions (helps developers determine where the issue was introduced)
    - ~crash for crashes and hard freezes
    - ~performance for slow/laggy performance and soft freezes
    - ~building for issues compiling the Inkscape program
    - ~packaging for issues with the packaging or problems specific to certain packaging formats (~packaging::AppImage, and ~packaging::snap for the exact package type)
    - ~OS::Windows, ~OS::Linux, ~OS::macOS, ~Wayland for issues specific to an OS or DE
- Requires specific action
    - ~"Request for comments" if requiring discussion from other contributors or experts.
    - ~"needs info" if the issue can't be solved without more information from the original reporter. They should be closed after 2 months if there is no response, but can be reopened later.
    - ~"needs reproducibility" if no one has been able to reproduce the problem yet and it needs other testers.
    - other labels starting with the phrase "needs".
    - ~"Possible duplicate" and ~"move candidate" for issues that may be duplicates or may be moved. Use them if you are unsure. Leave ~"Possible duplicate" labels on closed issues. It identifies duplicates when you're searching for issues in GitLab.
- ~"Closed::Expected Behavior" and other `Closed::*` labels describe why the issue was closed without fixing the issue.

Other bugtrackers use different labels. A link to all the project labels is present in the left sidebar of each project. This provides a short description of the label, and a link to a list of issues under that label. If you're in an issue, the label section in the right sidebar also lists all the labels in a drop-down.

## Milestones
Milestones indicate the Inkscape version that should have the fix. It is mainly used outside the Inbox bugtracker.

This is useful for open issues, as it prioritizes issues before a release. Don't add a milestone for every single issue, just the ones that really should be fixed by the next release.

This is also useful for closed issues, as it can help identify fixes when writing the Release Notes. Milestones may be removed in the future, so adding a comment helps too. Users may find the issue and wonder what version they need to avoid the bug.

A fix in the latest development version may be suitable for the next bugfix release. In that case, the issue should be left open, a milestone added for the bugfix release, and a `~backport::proposed` label added to the merge request that fixed the issue.

## Separating Out Issues
Each user report should focus on a single issue. The issue should be split out into multiple smaller issues. This should be done after a discussion of the issues. Users don't like seeing busywork being done without some acknowledgment of their problem.

It might be useful to track multiple issues all in one place. You could mark issues as related. Sometimes this is still insufficient. If so, keep the issue open and treat it like a GitLab epic. Split out each bug into its own issue and link to the bugs in the original issue.

The ~"needs separation" label can be used to mark issues that require splitting out.

## Confidential Issues
Confidential issues are only visible to a limited number of people (contributors with the Reporter role on GitLab, the author and assignees).

We don't recommend using confidential issues. For security issues, they aren't secure enough. For other issues, they reduce visibility too much.

Issues are usually marked as confidential because users are not comfortable sharing a logging file or example svg. Ideally the confidential information should be stripped from these files (and the edit history removed) so confidentiality can be removed. Users may also accidentally mark their issues confidential. They won't be able to remove confidentiality from their own issue.

We don't have a set procedure for dealing with security issues. If a security researcher requests to discuss an issue on GitLab, mention the request in the [developer chat](https://chat.inkscape.org/channel/team_devel) so developers are aware of the issue.

## Moving Issues

Bug Wranglers can move issues to any [project shared with the group](https://gitlab.com/groups/inkscape/bug-wranglers/-/shared). You can move an issue using the "Move issue" button at the bottom of the right sidebar, or using [quick actions](#quick-actions).

If you're not sure where the issue should go, read the description of each project or look at the issues in the bugtracker. See [Overview of Bugtrackers](#overview-of-bugtrackers) for a brief overview of common bugtrackers. You can also ask in the [chat](https://chat.inkscape.org/channel/team_testing) or label the issue a ~"move candidate".

**Conditions for moving a bug out of the Inbox:**

- There are no open duplicate issues in either bugtracker.
- The bug is confirmed (i.e. someone besides the original reporter can replicate the issue).
- The issue has been [tidied up](#4-clean-up-the-issue).
- The issue is labeled and will be labeled again after it has been moved.

**Special cases for moving issues:**

- Issues should be moved to the Inbox if they aren't confirmed. Sometimes issues are posted in the wrong place too (like Inkscape bug reports in inkscape-web or the vectors sub-projects).
- Some projects (like inkscape-web) prefer issues to be created directly in the relevant bugtracker instead of in the Inbox.
- [Feature requests](#addressing-feature-requests) are usually left in the Inbox.

On move, GitLab will close the current issue and open an identical issue in the appropriate project. You may want to lock the closed issue to prevent people accidentally commenting in the wrong place.

## Closing Issues
**Close duplicate issues**. This provides a single point of reference for the bug. The older issue should be kept, unless it was in our legacy bugtracker or the newer issue has significantly more relevant detail. Add any new information to the original issue. It can help to ask the user to add a :+1: to the original issue before closing their issue. It shows that they are affected by the issue and automatically turns their notifications on.

If you're unsure if it should be closed, you can mark the issue with the ~"Possible duplicate" label.

You can use [quick actions](#quick-actions) to mark the issue as a duplicate, which will close the issue and mark it as related to the original.

**Close abandoned issues** to keep the Inbox organized. These issues should already have a ~"needs info" label on them.

Abandoned issues:

- Lack enough information to triage or fix the issue,
- Have an unanswered request for more information from the original reporter or other person reporting the issue, and
- Are untouched for at least 2 months.

Add a comment explaining the close, and mention that the original reporter can reopen the issue if they still experience the bug, though they should provide more details.

**Close fixed issues**, leaving a comment mentioning the tested Inkscape version and the version of the next release containing the fix. If you can find it, add the commit or merge request that fixed the issue.

If you think a fix can be backported to the next bugfix release: leave the issue open, set the [milestone](#milestones) to the bugfix release and add a `~backport::proposed` label to the relevant merge request. Large changes or new features are usually unsuitable for backporting.

## Providing Support

Some issues are questions about how to use Inkscape, or requests for features that already exist. Bug reports often include a request to workaround the bug too. Help them out. If the issue is too complicated, redirect them to our [forum](https://inkscape.org/forums/) or [other support channels](https://inkscape.org/community/).

## Migrating Bugs
Bugs need to be moved from our legacy bugtracker on [launchpad](https://bugs.launchpad.net/inkscape/) to our current bugtracker on GitLab. See http://alpha.inkscape.org/bug-migration/ to learn how to migrate the issue.

# Tips and Tricks

## Searching
Tips to improve the search:

1. You can search all Inkscape bugtrackers at once by looking through the issues in the [Inkscape group project](https://gitlab.com/groups/inkscape/-/issues).
1. The legacy bugtracker on Launchpad can be filtered to [show issues that haven't been migrated yet](https://bugs.launchpad.net/inkscape/+bugs?field.searchtext=&field.status:list=NEW&field.status:list=CONFIRMED&field.status:list=TRIAGED&field.status:list=INPROGRESS&field.status:list=INCOMPLETE_WITH_RESPONSE&field.status:list=INCOMPLETE_WITHOUT_RESPONSE&assignee_option=any&field.assignee=&field.bug_reporter=&field.bug_commenter=&field.subscriber=&field.structural_subscriber=&field.tag=-bug-migration&field.tags_combinator=ANY&field.has_cve.used=&field.omit_dupes.used=&field.omit_dupes=on&field.affects_me.used=&field.has_patch.used=&field.has_branches.used=&field.has_branches=on&field.has_no_branches.used=&field.has_no_branches=on&field.has_blueprints.used=&field.has_blueprints=on&field.has_no_blueprints.used=&field.has_no_blueprints=on&search=Search&orderby=-id&start=0).
1. Searching by label usually decreases the number of search results significantly.
1. The Search bar at the top of the GitLab interface can be used to search for comments. This is generally slower, but can help find issues that you wouldn't otherwise catch.
1. Google search may also help in finding issues, filtered through the search term `site:https://gitlab.com/inkscape`. If you don't filter the search, you may also find useful advice on various forums, or on our legacy bugtracker.

## Formatting
GitLab uses [markdown](https://gitlab.com/help/user/markdown) to format comments and descriptions, with [syntax highlighting for code blocks](https://gitlab.com/help/user/markdown#colored-code-and-syntax-highlighting).

<details>
<summary>Using Expanders</summary>
You can also create an expander to hide details. Use the preview before posting as the expander may break markdown formatting. To fix, replace the markdown with the equivalent html.
<pre>
// some code
</pre>
</details>

```html
<details>
<summary>Using Expanders</summary>
You can also create an expander to hide details. Use the preview before posting as the expander may break markdown formatting. To fix, replace the markdown with the equivalent html.
<pre>
// some code
</pre>
</details>
```

GitLab also allows you to [link to merge requests and issues easily](https://gitlab.com/help/user/markdown#special-gitlab-references).

## Quick Actions
GitLab supports [quick actions](https://gitlab.com/help/user/project/quick_actions) for labeling and many other actions. This is usually quicker than using the sidebar.

Add the quick action on a separate line at the end of your comment when you are creating (not editing) it. GitLab should show a pop-up with an example of the command.

Common quick actions:

- `/close`
- `/duplicate inkscape#124` or `duplicate https://gitlab.com/inkscape/inkscape/-/issues/124`
- `/label ~bug ~Inkscape`
- `/move inkscape/inkscape`
- `/lock`
- `/title A new title for this issue`
- `/relate inkscape#124` or `/relate https://gitlab.com/inkscape/inkscape/-/issues/124`

## Checking User Preferences
Non-default user preferences or the presence of some dialogs may be required to trigger the bug.

Most preferences will be kept in the [user config folder](#finding-the-user-config-folder). Usually the issue is with `preferences.xml` or `dialogs-state-ex.ini` (or `dialogs-state.ini` in 1.1.x).

You should temporarily reset your preferences to ensure non-default settings aren't triggering the bug. If you're familiar with [environment variables](https://wiki.inkscape.org/wiki/Environment_variables), you can use INKSCAPE_PROFILE_DIR to change the location of the config folder.

### Backing up preferences.xml
Press the `Reset Preferences` button (found in the Preferences dialog, under the System tab). Restart Inkscape to see the changes. In older versions of Inkscape (<1.0), there is no `Reset Preferences` button.

Alternatively, close all instances of Inkscape, find `preferences.xml` and rename it to something else. It can be found in the [user config folder](#finding-the-user-config-folder).

To restore the old preferences:

1. Close all instances of Inkscape.
1. Delete the newly auto-generated `preferences.xml` file.
1. Rename the original preferences file back to `preferences.xml`. If you used the `Reset Preferences` button, the file name will be `preferences.xml` followed by the date and time (i.e. `preferences.xml_YYYY_MM_DD_HH_MM_SS`).

It is important to close all instances of Inkscape first, otherwise your preferences will be overwritten when Inkscape is closed. The `dialogs-state-ex.ini` can be restored in a similar way.

If the preferences are the issue, upload the offending preferences file.

### Finding the user config folder
The user config folder contains most of the user preferences and customization. Missing files are regenerated (based on the default template), so the whole folder can be safely removed.

The folder's location is listed in the Preferences dialog, under the System tab. It is also listed when `inkscape --user-data-directory` is run from the terminal.

It is usually:

- `~/.config/inkscape` on Linux,
- `%APPDATA%\inkscape` on Windows, or
- `~/Library/Application Support/org.inkscape.Inkscape/config/inkscape` on macOS.

## Collecting Backtraces
<a name="Collecting_Backtraces" /><!-- Legacy link -->
Backtraces help to debug crashes. Below are simple instructions for collecting backtraces without symbols.

It can also help to just run Inkscape in the terminal to log the warning or error messages (particularly if the backtrace reports a crash from a SIGTRAP). On Windows, you need to run `inkscape.com` from a terminal, instead of running `inkscape.exe`.

### Linux

The following will not work for Inkscape installed as a snap or flatpak.

1. Open the terminal.
2. Install `gdb`.
3. Run `gdb path/to/inkscape` (`gdb inkscape` may be sufficient).
4. Input `set pagination off` and press Enter. This makes the log cleaner.
5. Input `run` and press Enter. This should start Inkscape (it will run much slower).
6. Try to make Inkscape crash.
7. Switch back to gdb (you may need to do this using the keyboard).
8. Input `backtrace` and press Enter. This prints out the backtrace. Sometimes there isn't a backtrace.
9. Input `quit` and press Enter. This should exit gdb.
10. Copy the entire output to a text file. Make sure you copy the backtrace!

#### Frozen Inkscape
You can also use gdb to collect a backtrace from a frozen instance of Inkscape. You start gdb in the same way as when collecting a backtrace for a crash. When Inkscape freezes, switch back to gdb and use <kbd>Ctrl+C</kbd> before inputting `backtrace`. You can then input `continue`, <kbd>Ctrl+C</kbd>, then `backtrace` a few times to collect multiple backtraces.

### MacOS

Apple's Report Service should generate crash logs when Inkscape crashes. These may be inside an error dialog, or a file in `~/Library/Logs/DiagnosticReports` (named `inkscape_(date and time) (computer name).crash`). They are not always generated.

### Windows

Go to the folder where Inkscape is installed (e.g. `C:\Program Files\Inkscape\`), and run `Run Inkscape and create debug trace.bat`.

In 1.0.x, you will need to go to the bin folder (e.g. `C:\Program Files\Inkscape\bin`), and run `gdb_create_backtrace.bat`.

This launches a terminal and then Inkscape. Follow the instructions in the terminal, and upload the generated file (e.g. `C:\Users\MyUserName\inkscape_backtrace.txt`).

#### Frozen Inkscape

Process Explorer can collect backtraces if Inkscape is frozen or still running.

1. Download [Process Explorer](https://docs.microsoft.com/en-us/sysinternals/downloads/process-explorer) from Microsoft Sysinternals.
2. Launch Process Explorer (`procexp64.exe`).
3. Launch Inkscape.
4. Locate `inkscape.exe` in the Process Explorer list, right click and open Properties....
5. In the dialog that pops up, select the Threads tab
6. Take a screenshot of the threads list
7. Select the thread whose start address starts with `inkscape.exe+0x...` and click on the 'Stack' button
8. Post a screenshot of that second dialog that pops up

## Running Multiple Instances
In 1.2 and later, launching Inkscape multiple times no longer creates separate instances. Instead, the current instance of Inkscape will simply launch a new window (this saves memory and avoids race conditions when reading user preferences).

You can override this by running Inkscape with a unique `--app-id-tag`, e.g. `inkscape --app-id-tag second_instance`.

This will run into the race conditions mentioned above, as the instances will read and write to the same config folder. If you're familiar with [environment variables](https://wiki.inkscape.org/wiki/Environment_variables), you can use INKSCAPE_PROFILE_DIR to ensure each instance runs with its own config folder.